<!-- Footer -->
<div class="footer text-muted text-center">
	<?php echo $this->M_Web->footer(); ?>
</div>
<!-- /footer -->

</div>
<!-- /content area -->

</div>
<!-- /main content -->

</div>
<!-- /page content -->

</div>
<!-- /page container -->
<script>
                
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(successFunction, errorFunction);
        } else {
            alert('It seems like Geolocation, which is required for this page, is not enabled in your browser. Please use a browser which supports it.');
        }
                    
        function errorFunction() {
            
        }
        
        function successFunction(position) {
            var lat = position.coords.latitude;
            var long = position.coords.longitude;
            console.log('Your latitude is :'+lat+' and longitude is '+long);
        
			document.getElementById("map").href = "map?lat="+lat+"&lng="+long;

            // //ajax send lat long ke controller
            // var base_url = '<?php echo base_url() ?>';
        
            // $.ajax({
            //     url: base_url + "map/index",  // define here controller then function name
            //     method: 'POST',
            //     data: {
            //          lat: lat,
            //          lng: long 
            //          },    // pass here your date variable into controller
            //     success:function(result) {
            //         console.log('sukses bro') // alert your date variable value here
            //     }
            // });
        }
                
                
    </script>
</body>
</html>
