<?php $this->load->view('web/diagnosa/atas'); ?>
<div class="col-md-3"></div>
<div class="col-md-6">
  <form class="form-horizontal" action="/motormatic/diagnosa/simpangejala/" method="post" enctype="multipart/form-data">
    <div class="form-group">
      <label>Pilih Gejala</label>
      <select class="form-control" name="gejala_penentu">
        <?php foreach($list_gejala_penentu as $item => $val){ ?>
        <option value="<?php echo $val['kode_gejala'] ?>"><?php echo $val['nama_gejala'] ?></option>
        <?php } ?>
      </select>
    </div>
    <center><button type="submit" name="btnstep" class="btn btn-primary btn-lg">Jawab</button></center>
  </form>
</div>
<div class="col-md-3"></div>
<?php $this->load->view('web/diagnosa/bawah'); ?>
