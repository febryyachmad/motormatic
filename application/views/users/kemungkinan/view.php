<div class="content-wrapper">
	<div class="content">
		<?php
			echo $this->session->flashdata('msg');
		?>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-flat">
			        <div class="panel-heading">
						<h6 class="panel-title"><i class="icon-clipboard6"></i> Data <?php echo $judul_web; ?> <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			          	<div class="heading-elements">
				            <ul class="icons-list">
				                <li><a data-action="collapse"></a></li>
				            </ul>
			           	</div>
			        </div>
			        <hr style="margin:0px;">
			        <div class="panel-body">
			        	<a href="kemungkinan/tambah/" class="btn btn-success" style="margin-bottom: 10px;"><i class="icon-plus2"></i><?php echo $judul_web; ?> Baru</a>
			        	<div class="table-responsive">
			              	<table class="table datatable-basic table-bordered table-striped" width="100%">
				                <thead>
			                  		<tr>
					                    <th>No.</th>
					                    <th>Kode Gejala Inti</th>
					                    <th>Kode Gejala Saat Ini</th>
					                    <th>Kode Gejala Selanjutnya</th>
					                    <th>Kode Penyakit</th>
					                    <th>Bercabang</th>
					                    <th class="text-center">Aksi</th>
				                  	</tr>
				                </thead>
				                <tbody>
				                	<?php
					                    $no=1;
					                    foreach ($list_query_kemungkina->result() as $item) {
				                    ?>
				                    <tr>
				                    	<td><?php echo $no++.'.'; ?></td>
				                    	<td><?php echo $item->kode_gejala ?></td>
				                    	<td><?php echo $item->kode_gejala_sebelumnya ?></td>
				                    	<td><?php echo $item->kode_gejala_selanjutnya ?></td>
				                    	<td><?php echo $item->kode_penyakit ?></td>
				                    	<td class="text-center">
				                    		<?php 
				                    			if($item->bercabang == "Ya"){
				                    				echo '<i class="icon-checkmark bg-green-400"></i>';
				                    			}else{
				                    				echo '<i class="icon-cross bg-danger-400"></i>';
				                    			}
				                    		?>
				                    	</td>
				                    	<td class="text-center">
				                    		<a href="kemungkinan/edit/<?php echo $item->id_kemungkinan ?>" class="btn btn-primary btn-xs"><i class="icon-pencil7"></i></a>
				                    		<a href="kemungkinan/delete/<?php echo $item->id_kemungkinan ?>" class="btn btn-danger btn-xs"><i class="icon-trash"></i></a>
				                    	</td>
				                    </tr>
				                	<?php } ?>
				                </tbody>
				            </table>
				        </div>
			        </div>
				</div>
			</div>
		</div>
	</div>
</div>