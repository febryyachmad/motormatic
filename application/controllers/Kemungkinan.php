<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kemungkinan extends CI_Controller {
	function __construct() {
		parent::__construct();
		$level = $this->session->userdata('level');
		if ($level!='admin') {
			redirect('404');
		}
	}

	public function index(){
		$data['judul_web'] = "Kemungkinan";

		$data['list_query_kemungkina'] = $this->db->get("tbl_kemungkinan");
		$this->load->view('users/header', $data);
		$this->load->view('users/kemungkinan/view', $data);
		$this->load->view('users/footer');
	}

	public function tambah(){
		if($_POST){
			$kode_gejala = $this->input->post('kode_gejala');
			$kode_gejala_sebelumnya = $this->input->post('kode_gejala_sebelumnya');
			$kode_gejala_selanjutnya = $this->input->post('kode_gejala_selanjutnya');
			$kode_penyakit = $this->input->post('kode_penyakit');
			$bercabang = $this->input->post('bercabang');

			if($bercabang == "on"){
				$bercabang = "Ya";
			}else{
				$bercabang = "Tidak";
			}
			$data = array(
				'kode_gejala' => $kode_gejala,
				'kode_gejala_sebelumnya' => $kode_gejala_sebelumnya,
				'kode_gejala_selanjutnya'  => $kode_gejala_selanjutnya,
				'kode_penyakit'  => $kode_penyakit,
				'bercabang'  => $bercabang
			);
			$this->db->insert('tbl_kemungkinan',$data);
			$this->session->set_flashdata('msg',
				'
				<div class="alert alert-success alert-dismissible" role="alert">
					 <button type="button" class="close" data-dismiss="alert" aria-label="Close">
						 <span aria-hidden="true">&times;&nbsp; &nbsp;</span>
					 </button>
					 <strong>Sukses!</strong> Berhasil disimpan.
				</div>'
			);
			redirect('kemungkinan/tambah');
		}else{
			$data['judul_web'] = "Tambah Kemungkinan";
			$data['list_query_gejala'] = $this->db->get("tbl_gejala");
			$data['list_query_penyakit'] = $this->db->get("tbl_penyakit");
			$this->load->view('users/header', $data);
			$this->load->view('users/kemungkinan/tambah', $data);
			$this->load->view('users/footer');
		}
	}

	public function edit($id_kemungkinan = ''){
		if(!$id_kemungkinan){
			redirect('kemungkinan');
		}

		if($_POST){
			$kode_gejala = $this->input->post('kode_gejala');
			$kode_gejala_sebelumnya = $this->input->post('kode_gejala_sebelumnya');
			$kode_gejala_selanjutnya = $this->input->post('kode_gejala_selanjutnya');
			$kode_penyakit = $this->input->post('kode_penyakit');
			$bercabang = $this->input->post('bercabang');

			if($bercabang == "on"){
				$bercabang = "Ya";
			}else{
				$bercabang = "Tidak";
			}
			$data = array(
				'kode_gejala' => $kode_gejala,
				'kode_gejala_sebelumnya' => $kode_gejala_sebelumnya,
				'kode_gejala_selanjutnya'  => $kode_gejala_selanjutnya,
				'kode_penyakit'  => $kode_penyakit,
				'bercabang'  => $bercabang
			);
			$this->db->where('id_kemungkinan', $id_kemungkinan);
        	$this->db->update('tbl_kemungkinan', $data);
			$this->session->set_flashdata('msg',
				'
				<div class="alert alert-success alert-dismissible" role="alert">
					 <button type="button" class="close" data-dismiss="alert" aria-label="Close">
						 <span aria-hidden="true">&times;&nbsp; &nbsp;</span>
					 </button>
					 <strong>Sukses!</strong> Berhasil disimpan.
				</div>'
			);
			redirect("kemungkinan/edit/$id_kemungkinan");
		}else{
			$data['judul_web'] = "Edit Kemungkinan";
			$data['list_query_gejala'] = $this->db->get("tbl_gejala");
			$data['list_query_penyakit'] = $this->db->get("tbl_penyakit");
			$data['obj'] = $this->db->get_where('tbl_kemungkinan', array('id_kemungkinan' => $id_kemungkinan))->row();

			$this->load->view('users/header', $data);
			$this->load->view('users/kemungkinan/edit', $data);
			$this->load->view('users/footer');
		}
	}

	public function delete($id_kemungkinan = ''){
		if(!$id_kemungkinan){
			redirect('404');
		}

		$this->db->delete('tbl_kemungkinan', array('id_kemungkinan' => $id_kemungkinan));
		redirect('kemungkinan');
	}
}